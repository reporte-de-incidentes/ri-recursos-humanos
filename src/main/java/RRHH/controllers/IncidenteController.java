package RRHH.controllers;

import RRHH.service.IncidenteService;
import com.RIcommons.dto.IncidenteDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/incidentes")
public class IncidenteController
{
    @Autowired
    IncidenteService incidenteService;

    @GetMapping("/byTecnico/{id_tecnico}")
    public ResponseEntity<List<IncidenteDTO>> getIncidentesByTecnico(@PathVariable("id_tecnico") int id_tecnico){
        return new ResponseEntity<>(incidenteService.obtenerIncidentesPorTecnico(id_tecnico), HttpStatus.OK);
    }
}
