package RRHH.controllers;

import RRHH.service.TecnicoService;
import com.RIcommons.dto.TecnicoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/tecnicos")
public class TecnicoController
{
    @Autowired
    TecnicoService tecnicoService;

    @PostMapping("/save")
    public ResponseEntity<String> saveTecnico(@RequestBody TecnicoDTO tecnicoDTO){
        tecnicoService.guardarTecnico(tecnicoDTO);
        return ResponseEntity.ok("El tecnico ha sido cargado con exito");
    }

    @PutMapping("/modify/{id_tecnico}")
    public ResponseEntity<String> modifyTecnico(@PathVariable("id_tecnico") int id_tecnico,
                                                @RequestBody TecnicoDTO tecnicoDTO){
        tecnicoService.modificarTecnico(tecnicoDTO);
        return ResponseEntity.ok("El tecnico ha sido modificado con exito");
    }

    @DeleteMapping("/delete/{id_tecnico}")
    public ResponseEntity<String> deleteTenico(@PathVariable("id_tecnico") int id_tecnico){
        tecnicoService.eliminarTecnico(id_tecnico);
        return ResponseEntity.ok("El tecnico ha sido eliminado con exito");
    }

}
