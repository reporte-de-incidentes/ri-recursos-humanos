package RRHH;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EntityScan(basePackages = "com.RIcommons.entity")
@ComponentScan(basePackages = "com.RIcommons.assembler")
public class RecursosHumanosApplication {
	public static void main(String[] args) {
		SpringApplication.run(RecursosHumanosApplication.class, args);
	}

}
