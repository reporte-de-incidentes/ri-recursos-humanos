package RRHH.service;

import RRHH.repository.IIncidenteRepository;
import com.RIcommons.assembler.DTOsAssembler;
import com.RIcommons.dto.IncidenteDTO;
import com.RIcommons.entity.Incidente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;


import java.util.ArrayList;
import java.util.List;

@Service
public class IncidenteService
{
    @Autowired
    IIncidenteRepository iIncidenteRepository;
    @Autowired
    DTOsAssembler dtOsAssembler;

    public List<IncidenteDTO> obtenerIncidentesPorTecnico(int id_tecnico){
        List<Incidente> incidenteList = iIncidenteRepository.findIncidenteByIdTecnico(id_tecnico);
        List<IncidenteDTO> incidenteDTOList = new ArrayList<>();
        if (!ObjectUtils.isEmpty(incidenteList)){
            for (Incidente inc : incidenteList){
                incidenteDTOList.add(dtOsAssembler.incidenteDTO(inc));
            }
        }
        return incidenteDTOList;
    }

}
