package RRHH.service;

import RRHH.repository.ITecnicoRepository;
import com.RIcommons.assembler.EntityAssembler;
import com.RIcommons.dto.TecnicoDTO;
import com.RIcommons.entity.Tecnico;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TecnicoService
{
    @Autowired
    ITecnicoRepository tecnicoRepository;

    @Autowired
    EntityAssembler entityAssambler;

    public void guardarTecnico(TecnicoDTO tecnicoDTO){
        Tecnico tecnico = entityAssambler.toTecnico(tecnicoDTO);
        tecnicoRepository.save(tecnico);

    }

    public void modificarTecnico(TecnicoDTO tecnicoDTO){
        Tecnico tecnico = tecnicoRepository.findById(tecnicoDTO.getNro_tecnico()).orElseThrow();
        tecnico.setNombre(tecnicoDTO.getNombre());
        tecnico.setEmail(tecnicoDTO.getEmail());
        tecnico.setNro_contacto(tecnicoDTO.getNro_contacto());

        tecnicoRepository.save(tecnico);
    }

    public void eliminarTecnico(int id_tecnico){
        tecnicoRepository.deleteById(id_tecnico);
    }
}
