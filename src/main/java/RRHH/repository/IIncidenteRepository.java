package RRHH.repository;

import com.RIcommons.entity.Incidente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IIncidenteRepository extends JpaRepository<Incidente, Integer>
{
    @Query("SELECT i FROM Incidente i WHERE i.tecnico.id = :id_tecnico")
    List<Incidente> findIncidenteByIdTecnico(@Param("id_tecnico") int id_tecnico);


}
